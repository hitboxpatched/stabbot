#!/usr/bin/env python

import subprocess
import os

from helper import PicnicException

class uploader:
    prismedia = '/prismedia/prismedia_upload.py'
    peertube_secret_dir = '/bot/peertube_secret_dir/'

    @staticmethod
    def geturl(s):
        # the output contains much more than just the url
        cue = "Peertube: Watch it at "
        for line in s.split("\n"):
            if cue in line:
                # the url befins right after the cue. Between in url and the end of the line is a single ".",
                # which also needs to be remove
                return line.split(cue, 1)[1][:-1]

    def upload(self, file, name="no_name", nsfw=True, category="Entertainment",
               description="no description\nline1\n\nline3", tags="", privacy="unlisted"):

        file = os.path.abspath(file)
        cwd = os.getcwd()

        parameter = [self.prismedia,
                     "-f", file,
                     "--platform=peertube",
                     "--name", name,
                     "--category", category,
                     "--description", description,
                     "--tags", tags,
                     "--privacy", privacy
                     ]

        if nsfw:
            parameter.append("--nsfw")

        try:
            # the working dir for vid.stab get deleted after every iteration,
            # so peertube_secret must be in some other location.
            # but it must also be in cwd for prismedia to fine it
            os.chdir(self.peertube_secret_dir)
            output = subprocess.check_output(
                parameter,
                stderr=subprocess.STDOUT)

            return uploader.geturl(output)

        except subprocess.CalledProcessError as cpe:
            print "cpe.returncode", cpe.returncode
            print "cpe.cmd", cpe.cmd
            print "cpe.output", cpe.output

            raise PicnicException("couldn't upload to peertube")

        finally:
            os.chdir(cwd)


# p = uploader()
# print(p.upload("a.mp4"))
